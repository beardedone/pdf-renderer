# PDF Renderer
PDF Renderer which runs on port `:7900`

## Request
```
{
    "HtmlBody",
    "DPI",
    "Height",
    "Width",
    "MarginTop",
    "MarginBottom",
    "MarginLeft",
    "MarginRight"
}
```

## Response
```
{
    "data"
}
```