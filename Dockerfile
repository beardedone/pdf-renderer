FROM golang:alpine AS builder

RUN apk --no-cache add ca-certificates git
WORKDIR /usr/src/app

# libraries
RUN go get github.com/SebastiaanKlippert/go-wkhtmltopdf && \
    go get github.com/gin-gonic/gin

COPY . .
RUN CGO_ENABLED=0 GOOS=linux go build -v -a -installsuffix cgo -o app .

FROM scratch
FROM surnet/alpine-wkhtmltopdf:3.7-0.12.4-small

LABEL Author="Michael K. Essandoh <mexcon.mike@gmail.com>"

COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/

RUN mkdir -p /pdf-renderer
WORKDIR /pdf-renderer

EXPOSE 7900

COPY --from=builder /usr/src/app/app .
ENTRYPOINT ["./app"]